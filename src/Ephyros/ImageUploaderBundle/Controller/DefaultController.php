<?php

namespace Ephyros\ImageUploaderBundle\Controller;

use Ephyros\ImageUploaderBundle\Entity\Image;
use Ephyros\ImageUploaderBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $images = $user->getImages();

        $image = new Image();
        $form = $this->createFormBuilder($image)
            ->add('file')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {
            $image->setUser($user);

            $em = $this->getDoctrine()->getManager();
            $em->persist($image);
            $em->flush();

            return $this->redirect($this->generateUrl('ephyros_image_uploader_homepage'));
        }

        return $this->render('EphyrosImageUploaderBundle:Default:index.html.twig', array(
            'images' => $images,
            'form' => $form->createView()
        ));
    }
}
