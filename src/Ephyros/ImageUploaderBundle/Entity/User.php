<?php

namespace Ephyros\ImageUploaderBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="fos_user")
 * @ORM\Entity
 */
class User extends BaseUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @ORM\OneToMany(targetEntity="Image", mappedBy="user")
     */
    protected $images;

    public function __construct()
    {
        parent::__construct();
        $this->images = new ArrayCollection();
    }

    /**
     * Add images
     *
     * @param \Ephyros\ImageUploaderBundle\Entity\Image $images
     * @return User
     */
    public function addImage(\Ephyros\ImageUploaderBundle\Entity\Image $images)
    {
        $this->images[] = $images;
    
        return $this;
    }

    /**
     * Remove images
     *
     * @param \Ephyros\ImageUploaderBundle\Entity\Image $images
     */
    public function removeImage(\Ephyros\ImageUploaderBundle\Entity\Image $images)
    {
        $this->images->removeElement($images);
    }

    /**
     * Get images
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getImages()
    {
        return $this->images;
    }
}